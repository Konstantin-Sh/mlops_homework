(Русская версия внизу)

# Repository Description

This repository is created for completing homework assignments as part of the MLOps course at ods.ai.
https://ods.ai/tracks/mlops3-course-spring-2024

## Repository Management Methodology

At the initial stage, this repository plans to use the Github Flow methodology for branch management.

Main branches:

- `main` - branch with the latest stable version of the project.
- `develop` - branch for developing new features.
- Each research in its own separate branch.

Branch management:

- For each research, a separate branch is created from `develop`.
- After successful completion of the research, the branch is merged back into `develop`.
- Releases are made from `develop` to `main`.

## Package Manager

Package management is handled using the pixi package manager.

To install the necessary dependencies, execute:

```bash
pixi install
```

## Building Docker Image

```bash
docker build -t mlops-homework .
```

## Running Docker Container

```bash
docker run -it --rm mlops-homework
```

---
Этот репозитория создан для прохождения домашних заданий в рамках курса MLOps на ods.ai
https://ods.ai/tracks/mlops3-course-spring-2024

## Методология ведения репозитория

На начальном этапе в этом репозитории планируется использовать методологию Github Flow для организации работы с ветками. 

Основные ветки:
- `main` - ветка с последней стабильной версией проекта.
- `develop` - ветка для разработки новых функций.
- Каждое исследование в своей отдельной ветке

Работа с ветками:
- Для каждого исследования создается отдельная ветка от `develop`.
- После успешного завершения исследования ветка сливается обратно в `develop`.
- Выпускаются релизы из `develop` в `main`.

## Пакетный менеджер

Управления пакетами происходит с помощью пакетного менеджера pixi

Для установки необходимых зависимостей выполните:

```bash
pixi install
```

## Сборка Docker образа

```bash
docker build -t mlops-homework .
```

## Запуск Docker контейнера

```bash
docker run -it --rm mlops-homework
```