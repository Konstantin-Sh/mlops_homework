FROM debian:bookworm-slim

RUN apt update \
    && apt install -y curl tar \
    && rm -rf /var/lib/apt/lists/* \
    && curl -fsSL https://pixi.sh/install.sh | bash \
    && mv /root/.pixi/bin/pixi /usr/local/bin/pixi

COPY pixi.toml /app/pixi.toml

WORKDIR /app

RUN pixi install

COPY . /app

ENTRYPOINT ["pixi","run","python","-c print('hello world')"]
