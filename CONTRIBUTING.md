(Русская версия внизу)
## Using Linters

In this project, ruff is used for code validation:

With rules: Pyflakes (`F`), pycodestyle (`E`), pycodestyle warnings (`W`), McCabe complexity (`C901`)

"E4", "E7", "E9", "F", "B", "W", "C901"

Additionally, mypy is used for type checking.

Before committing to the repository, it is recommended to run `pre-commit` to automatically validate and correct the code.

To install the necessary dependencies, execute:

```bash
pixi install
```

To run the validation, execute:
```bash
pre-commit run --all-files
```

## Использование линтеров

В этом проекте используются ruff для проверки кода:

С правилами:  Pyflakes (`F`), pycodestyle (`E`), pycodestyle warnings (`W`), McCabe complexity (`C901`)

"E4", "E7", "E9", "F", "B", "W", "C901"

А также mypy для типизации.

Перед коммитом в репозиторий рекомендуется запускать `pre-commit`, чтобы автоматически проверить код на соответствие и исправить его.

Для установки необходимых зависимостей выполните:

```bash
pixi install
```

Для запуска проверки выполните:
```bash
pre-commit run --all-files
```
